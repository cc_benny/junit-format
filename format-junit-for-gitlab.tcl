#!/usr/bin/tclsh
# ------------------------------------------------------------------------
#   format-junit-for-gitlab.tcl
#
#   Get the data from all Gitlab jobs for a project.  Collect the
#   JUnit reports if any and generate an overview page.
#
#   General rules:
#
#   * Some subcommands query gitlab.com, others get a list of jobs
#     from stdin.
#
#   * All subcommands return their output on stdout.
#
#   * There are two backend implementations, `api-backend.tcl' which
#     gets the data via the Gitlab API, but requires authentication.
#     And `graphql-backend' which uses GraphQL, and works without
#     authentication when the project is public.  We used to have a
#     scraping backend, but that does not work any more since the
#     official web site uses GraphQL.
#
#   * The backends implement subcommands to get jobs and they
#     implement the function `get-unit-xml'.
#
#   * This file implements generic subcommands, utility functions for
#     the backends and the command-line parsing.
# ------------------------------------------------------------------------

package require tdom

variable basedir [file dirname [info script]]

# parse-options argv - Parse environment and the command-line args
# given for options.  Return the remaining command-line args that
# could not be parsed.
#
# In detail:
#
# * Take the first parameter as the name of a backend,
#   "graphql-backend" or "api-backend" and source that backend.
#
# * For all options that are in the global `options' array (see the
#   backends which those are), convert them into the usual environment
#   variable format (uppercase with underscores "_" instead of hyphens
#   "-") and check if they are set.
#
# * For all leading parameter pairs of the form "--key value" convert
#   the key into the corresponding option key and set those.
#
# * Return the rest of the parameters.
proc parse-options {argv} {

    # Load the backend, so we know which options we have.
    variable basedir
    set argv [lassign $argv backend]
    if {"" ne $backend} {
        # Allow without ".tcl" for purity and with ".tcl" to be used
        # with shell auto-complete.
        if {! [string match "*.tcl" $backend]} {
            append backend .tcl
        }
        if {! [file exists $backend]} {
            set backend [file join $basedir $backend]
        }
        if {! [file exists $backend]} {
            error "First parameter must be a valid backend file"
        }
        uplevel #0 source $backend
    }

    # Set options from environment variables.
    variable options
    foreach option [array names options] {
        set eoption [string map {- _} [string toupper $option]]
        if {[info exists ::env($eoption)]} {
            set options($option) $::env($eoption)
        }
    }

    # Set options from the command line.
    while {[string match "-*" [lindex $argv 0]]} {
        set argv [lassign $argv option value]
        regsub {^--?} $option "" option
        if {! [info exists options($option)]} {
            error "--$option: Unknown option"
        }
        set options($option) $value
    }

    # Return what is left.
    return $argv
}

# write-file fname c - Write the new file FNAME with the string C as
# contents.
proc write-file {fname c} {
    set ofs [open $fname wb]
    puts -nonewline $ofs $c
    close $ofs
}

# read-file fname - Read the file FNAME and return the contents.  Can
# also be used to run a pipe in the usual Tcl manner, see open(3tcl).
proc read-file {fname} {
    set ifs [open $fname rb]
    set c [read $ifs]
    close $ifs
    return $c
}

# generate-local-job - A subcommand to generate a job object
# describing the Gitlab job in which we are currently running.
proc generate-local-job {} {
    variable options
    set now [clock format [clock seconds] \
                 -format "%Y-%m-%dT%H:%M:%SZ" -gmt 1]
    return [list [list id current \
                      created_at $now \
                      name       $::env(CI_JOB_NAME) \
                      ref        $::env(CI_COMMIT_REF_NAME) \
                      commit_sha $::env(CI_COMMIT_SHORT_SHA) \
                      web_url    $::env(CI_JOB_URL) \
                      junit_file $options(junit-file) \
                      report-url $options(html-file)]]
}

# jobs-list - A subcommand that takes a list of jobs from stdin and
# formats it one job per line.  This is used to get an overview and
# also to count jobs, just by using `wc -l' on the result.
proc jobs-list {} {
    foreach job [read stdin] {
        # name: The job label in .gitlab-ci.yml.
        # ref: Branch name
        puts "[dict get $job created_at]\
              [dict get $job id]\
              [dict get $job name]\
              [dict get $job ref]\
              [dict get $job commit_sha]\
              [dict get $job web_url]"
    }
}

# jobs-dump - A subcommand that takes a list of jobs from stdin and
# formats them with one key-value pair per line.
proc jobs-dump {} {
    foreach job [read stdin] {
        unset -nocomplain ""
        array set "" $job
        parray ""
        puts ""
    }
}

# generate-overview - A subcommand that takes a list of jobs from
# stdin and generates a simple XML file from it.  The result can be
# used with `format-junit.xslt' to generate an HTML file giving an
# overview of all the jobs.
proc generate-overview {} {
    variable options

    foreach job [read stdin] {
        #puts >>$job<<
        set report_url \
            [dict get $job web_url]/artifacts/$options(html-file)
        catch {set report_url [dict get $job report-url]}
        set summary \
            "    <summary\
                         time='[dict get $job created_at]'\
                           id='[dict get $job id]'\
                         name='[dict get $job name]'\
                          url='[dict get $job web_url]'\
                   report-url='$report_url'"

        if {[catch {
            if {[dict exists $job junit_file]} {
                # This is for a locally simulated job.
                set junitxml [read-file [dict get $job junit_file]]
            } else {
                # This is for a job as given by a backend.
                set junitxml [get-junit-xml $job]
            }
            #write-file junit-[dict get $job id].xml $junitxml
            set doc [dom parse $junitxml]
        } eresult]} {
            puts stderr "[dict get $job id]: $eresult"
            continue
        }

        foreach {label xpath} {
            all     {count(//testcase)}
            errors  {count(//testcase[error|failure])}
            skipped {count(//testcase[skipped])}
            passed  {count(//testcase[not(error|failure|skipped)])}
        } {
            set count [$doc selectNodes $xpath]
            append summary " $label='$count'"
            set count_$label $count
        }

        rename $doc ""

        # Remember the earliest entry for each commit ref.  This is so
        # that we can omit secondary executions of the same commit,
        # created e.g. through job scheduling.  Only do this if the
        # same number of tests actually passed, to deal with a
        # changing environment.  Also consider the job name, so we do
        # not confuse the jobs running for the same commit.
        set key [dict get $job name]/[dict get $job commit_sha]/$count_passed
        set created_at [dict get $job created_at]
        if {! [info exists firsts($key)] || "$firsts($key)" > "$created_at"} {
            set firsts($key) $created_at
        }

        set ref [dict get $job ref]
        set time [dict get $job created_at]

        append summary " />\n"
        lappend branches($ref) $key $firsts($key) $summary

        if {! [info exists dates($ref)] || $dates($ref) < $time} {
            set dates($ref) $time
        }
    }
    set xml "<overview>\n"

    # Filter out duplicates, s.a.
    foreach {ref branch} [array get branches] {
        foreach {key first_created summary} $branch {
            if {$first_created eq $firsts($key)} {
                append summaries_per_branch($ref) $summary
            }
        }
    }
    # Order the branches latest first.  Ordering of jobs within the
    # branches is done in format-junit.xslt.
    set ordereddates [lsort -stride 2 -index 1 -decreasing [array get dates]]
    foreach {branch _} $ordereddates {
        set summaries $summaries_per_branch($branch)
        append xml "  <branch name='$branch'>\n"
        append xml      $summaries
        append xml "  </branch>\n"
    }
    append xml "</overview>\n"
    return $xml
}

# Parse options, run the rest of the command-line args as a subcommand
# and print the result.
puts -nonewline [eval [parse-options $argv]]

# ------------------------------------------------------------------------
#   eof
# ------------------------------------------------------------------------
