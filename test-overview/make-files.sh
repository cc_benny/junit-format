#!/bin/bash
# ------------------------------------------------------------------------
#   make-files.sh
#
#   Generate test files, reports and overview.  This just documents
#   how to get test data for the major functions and it also makes
#   minor adjustments to the files to create a testing environment.
# ------------------------------------------------------------------------

set -e  # Exit on error.

SCRIPT="../format-junit-for-gitlab.tcl graphql-backend"

function get-jobs () {
    (
        $SCRIPT get-jobs-with-artifacts
        echo
        CI_COMMIT_REF_NAME=ref \
        CI_JOB_NAME=test \
        CI_JOB_URL=. \
        CI_COMMIT_SHORT_SHA=aaaaaa \
                  $SCRIPT generate-local-job
        echo
    ) > jobs.tcl
}

function get-reports () {
    $SCRIPT get-artifacts test-report.html < jobs.tcl

    # Generate HTML for the "local" job.
    xsltproc --param generate-summary false \
             --stringparam job-id test \
             --stringparam job-url test-report.xml \
             ../format-junit.xslt test-report.xml \
             > test-report.html
}

function make-overview-xml () {
    # sed: Refer to the local files that are created by get-reports.
    $SCRIPT generate-overview < jobs.tcl \
            | sed "s:report-url='[^=]*/-/jobs/:report-url=':;
                   s:/artifacts/test-report.html:-test-report.html:" \
            > overview.xml
}

function make-overview-html () {
    xsltproc --stringparam overview-title test-overview \
             ../format-junit.xslt overview.xml > overview.html
}

function run-html () {
    xdg-open overview.html
}

function make-all () {
    get-jobs
    get-reports
    make-overview-xml
    make-overview-html
}

cd $(dirname "$0")
"$@"

# ------------------------------------------------------------------------
#   eof
# ------------------------------------------------------------------------
