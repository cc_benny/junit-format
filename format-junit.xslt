<?xml version="1.0"?>
<!-- =================================================================
  format-junit.xslt

  A stylesheet to format a JUnit file as produced by Tclunit into a
  reasonable but fairly minimal HTML.  Produces colored bars for
  overviews and interactive show/hide for opening individual test
  suites and test failures.

  Can also be run with an overview XML as produced by
  `format-junit-for-gitlab.tcl' to generate an overview HTML page,
  showing bars for all JUnit jobs in a Gitlab project and links to the
  corresponding detail pages.
================================================================== -->
<xsl:stylesheet
    version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output
      encoding="utf-8" indent="yes" method="html"
      doctype-public="-//W3C//DTD HTML 4.0 Transitional//EN" />

  <xsl:param name="report-title">JUnit Report</xsl:param>
  <xsl:param name="overview-title">JUnit Reports</xsl:param>
  <xsl:param name="generate-summary">true</xsl:param>
  <xsl:param name="job-id" />
  <xsl:param name="job-url" />

  <!-- JUnit XML: Toplevel.  -->
  <xsl:template match="/testsuites">
    <html>
      <head>
        <meta http-equiv="Content-type"
              content="text/html; charset=utf-8"></meta>
	<title><xsl:value-of select="$report-title" /></title>
	<xsl:call-template name="css" />
	<xsl:call-template name="javascript" />
      </head>

      <body onload="setupOpeners()">
        <!-- A link to the Gitlab UI.  -->
        <xsl:if test="$job-id and $job-url">
          <a class="job-link" target="_top" href="{$job-url}"
             >Job #<xsl:value-of select="$job-id" /></a>
        </xsl:if>
        <xsl:if test="$generate-summary">
	  <h1><xsl:value-of select="$report-title" /></h1>

	  <!-- Show a bar for all test cases.  -->
	  <h2>All Tests: <xsl:value-of select="count(//testcase)" /></h2>
	  <xsl:call-template name="bar" />
        </xsl:if>

	<!-- Show bars for all test suites and details for all failed
	     tests.  -->
	<h2>Test Suites</h2>
	<xsl:apply-templates />

	<hr />
      </body>
    </html>
  </xsl:template>

  <!-- Overview XML: Toplevel.  -->
  <xsl:template match="/overview">
    <html>
      <head>
        <meta http-equiv="Content-type"
              content="text/html; charset=utf-8"></meta>
	<title><xsl:value-of select="$overview-title" /></title>
	<xsl:call-template name="css" />
	<xsl:call-template name="javascript" />
      </head>

      <body onload="formatLabels(); activateFirstReport()">
	<h1><xsl:value-of select="$overview-title" /></h1>
	<xsl:apply-templates />
        <iframe onload="setIframeHeight(this)"
                onerror="setIframeHeight(this);"
                frameborder="0"
                style="height: 4000px; width: 100%;" />
      </body>
    </html>
  </xsl:template>

  <!-- Generate a CSS section.  -->
  <xsl:template name="css">
    <style type="text/css">
      <!-- General layout.  The combined "margin" property means "top
           right bottom left".  -->
      <!-- h2: Sections.  -->
      h2  { margin: 1.5em 0 .5em 0; }
      <!-- h3, hr: Test suites.  -->
      h3  { margin: 0 1em 0 1em; }
      hr  { margin: .5em 1em .5em 1em; }
      <!-- h4: Test cases.  -->
      h4  { margin: .5em 0 0 2em; }
      <!-- pre: Error details.  -->
      pre { margin-left: 4em; }
      th  { text-align: left; white-space: nowrap; }

      <!-- Setup for the colored bars.  -->
      .bar { padding: 0 1em 0 1em; }
      .passed-bar   { background-color: #0F0; height: 1em; }
      .error-bar    { background-color: #FAA; height: 1em; }
      .skipped-bar  { background-color: #CCF; height: 1em; }
      .non-test-bar { background-color: #EEE; height: 1em; }

      <!-- Tools for the Javascript openers.  -->
      .opener { cursor: pointer; }
      .closed { display: none; }

      <!-- A link to the Gitlab UI.  -->
      .job-link { display: block; float: right; }

      <!-- Activate the overview lines.  -->
      .bar-container table { cursor: pointer; border: 1px solid transparent; }
      .active-bar table { border: 1px solid black !important; }
    </style>
  </xsl:template>

  <!-- Generate a Javascript section with code to open and close HTML
       parts and other helpers.  -->
  <xsl:template name="javascript">
    <!-- A very simple library for opening and closing HTML elements.
         -->
    <script type="text/javascript">
      <![CDATA[
        // @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-v3-or-Later

        // --------------------
        // Open and close HTML parts.

        // Button text, also used to detect current status.
        var CLOSED = "\u25B8"; // ▸ Left-pointing triangle.
        var OPENED = "\u25BE"; // ▾ Down-pointing triangle.

        // Initialize all the buttons.
        function setupOpeners () {
          var openers = document.getElementsByClassName("opener");
          for (var opener in openers) {
            setupOpener(openers[opener]);
          }
        }

        // Initialize a single button.
        function setupOpener (opener) {
          opener.innerHTML = CLOSED;
          opener.onclick = function () { toggleOpen(opener); };
        }

        // Actually handle a button click.
        function toggleOpen (opener) {
          // Cut prefix "opener-" off the button id.
          var id = opener.id.substr(7);
          // The target has the same id, just with the prefix
          // "target-".
          var target = document.getElementById("target-" + id);

          if (CLOSED == opener.innerHTML) {
            opener.innerHTML = OPENED;
            target.classList.remove("closed");
          } else {
            opener.innerHTML = CLOSED;
            target.classList.add("closed");
          }
        }

        // --------------------
        // Format dates for local timezone.

        // Format a number with a fixed number of digits.
        function formatNumber (n, width) {
          var result = "" + n;
          while (result.length < width) {
            result = "0" + result;
          }
          return result;
        }

        // Format a date in pseudo ISO format and local timezone.
        function formatDate (date) {
          date = new Date(date);
          return "" +
             date.getFullYear() + "-" +
             formatNumber(date.getMonth()+1, 2) + "-" +
             formatNumber(date.getDate(), 2) + " " +
             formatNumber(date.getHours(), 2) + ":" +
             formatNumber(date.getMinutes(), 2)
             ;
        }

        // Run `formatDate' on all relevant HTML bits.
        function formatLabels () {
          var labels = document.getElementsByTagName("th");
          for (var label in labels) {
            label = labels[label];
            try {
              label.innerHTML = formatDate(label.innerHTML);
            } catch (e) {
            }
          }
        }

        // --------------------
        // Auto-resizing of an <iframe>

        // From https://stackoverflow.com/questions/9975810
        function getDocHeight(doc) {
          // https://stackoverflow.com/questions/1145850
          var body = doc.body, html = doc.documentElement;
          var height = Math.max( body.scrollHeight, body.offsetHeight,
            html.clientHeight, html.scrollHeight, html.offsetHeight );
          return height;
        }

        function setIframeHeight(iframe) {
          var doc = iframe.contentDocument || iframe.contentWindow.document;
          iframe.style.visibility = 'hidden';
          // Reset to minimal height.  Why does shrinking not work
          // without this?
          iframe.style.height = "10px";
          iframe.style.height = getDocHeight(doc) + "px";
          iframe.style.visibility = 'visible';
        }

        // --------------------
        // Activating a report in the <iframe>.

        var currentlyActiveBar;
        function activateReport (bar, src) {
          if (currentlyActiveBar) {
            currentlyActiveBar.classList.remove('active-bar');
          }
          currentlyActiveBar = bar;
          if (currentlyActiveBar) {
            currentlyActiveBar.classList.add('active-bar');
          }
          var iframe = document.getElementsByTagName("iframe")[0];
          iframe.style.height = 0;
          iframe.src = src || bar.getAttribute('report');
        }

        function activateFirstReport () {
          var bar = document.getElementsByClassName("bar-container")[0];
          activateReport(bar);
        }

        // @license-end
      ]]>
    </script>
  </xsl:template>

  <!-- Ignore text outside of what we show explicitly.  -->
  <xsl:template match="text()" />

  <!-- JUnit XML: Display a test suite.  -->
  <xsl:template match="testsuite[testcase]">
    <hr />
    <xsl:call-template name="bar" />
    <h3>
      <xsl:value-of select="@name" />
      (<xsl:value-of select="count(testcase)" /> Tests)
      <xsl:if test="*/error|*/failure" >
	<xsl:value-of select="' '" />
	<span class="opener" id="opener-{generate-id()}" />
      </xsl:if>
    </h3>
    <div id="target-{generate-id()}" class="closed">
      <xsl:apply-templates />
    </div>
  </xsl:template>

  <!-- JUnit XML: Display a failed test case.  -->
  <xsl:template match="error|failure">
    <h4>
      <xsl:value-of select="../@name" />
      <xsl:value-of select="' '" />
      <span class="opener" id="opener-{generate-id()}" />
    </h4>
    <pre id="target-{generate-id()}" class="closed"
	 ><xsl:value-of select="text()" /></pre>
  </xsl:template>

  <!-- JUnit XML: Display a colored bar showing passed/error/skipped.
       Uses the current element as the anchor and counts everything
       below that.  Details are in the mouse-over titles only.  -->
  <xsl:template name="bar">
    <xsl:variable name="testcases" select=".//testcase" />

    <xsl:call-template name="bar-table">
      <xsl:with-param
          name="errorCount"   select="count($testcases[error|failure])"/>
      <xsl:with-param
          name="skippedCount" select="count($testcases[skipped])"/>
      <xsl:with-param
          name="passedCount"  select="count($testcases[
					        not(error|failure|skipped)])"/>
    </xsl:call-template>
  </xsl:template>

  <!-- The part of template `bar' that does the output, but taking the
       parameters via parameters.  -->
  <xsl:template name="bar-table">
    <xsl:param name="label" />
    <xsl:param name="errorCount" />
    <xsl:param name="skippedCount" />
    <xsl:param name="passedCount" />

    <!-- The number of tests here.  Never passed, but used as default
         for $maxCount.  -->
    <xsl:param name="testCount"
               select="$errorCount + $skippedCount + $passedCount" />
    <!-- The maximum number of tests in any of the bars in this table.
         May not be passed, if there is only one bar.  -->
    <xsl:param name="maxCount"
               select="$testCount" />

    <xsl:variable name="nonTestCount"
                  select="$maxCount - $testCount" />

    <!-- Variables $*Percent: What we show in the tooltips, percent of
         $testCount.  -->
    <xsl:variable name="passedPercent"
		  select="$passedCount  * 100 div $testCount" />
    <xsl:variable name="errorPercent"
		  select="$errorCount   * 100 div $testCount" />
    <xsl:variable name="skippedPercent"
		  select="$skippedCount * 100 div $testCount" />

    <!-- Variables $*Width: The width of the bar parts, percent of
         $maxCount.  -->
    <xsl:variable name="passedWidth"
		  select="$passedCount  * 100 div $maxCount" />
    <xsl:variable name="errorWidth"
		  select="$errorCount   * 100 div $maxCount" />
    <xsl:variable name="skippedWidth"
		  select="$skippedCount * 100 div $maxCount" />
    <xsl:variable name="nonTestWidth"
		  select="$nonTestCount * 100 div $maxCount" />

    <table width="100%" class="bar">
      <tr>
	<xsl:if test="$label">
	  <th width="1%"><xsl:value-of select="$label" /></th>
        </xsl:if>
	<xsl:if test="$passedCount > 0">
	  <td class="passed-bar"
	      width="{$passedWidth}%"
	      title="Passed: {$passedCount}, {round($passedPercent)}%" />
	</xsl:if>
	<xsl:if test="$errorCount > 0">
	  <td class="error-bar"
	      width="{$errorWidth}%"
	      title="Errors: {$errorCount}, {round($errorPercent)}%" />
	</xsl:if>
	<xsl:if test="$skippedCount > 0">
	  <td class="skipped-bar"
	      width="{$skippedWidth}%"
	      title="Skipped: {$skippedCount}, {round($skippedPercent)}%" />
	</xsl:if>
	<xsl:if test="$nonTestCount > 0">
	  <td class="non-test-bar"
	      width="{$nonTestWidth}%"
	      title="" />
	</xsl:if>
      </tr>
    </table>
  </xsl:template>

  <!-- Overview XML: Format all jobs in a branch.  -->
  <xsl:template match="branch">
    <h2>Branch <xsl:value-of select="@name" /></h2>
    <xsl:apply-templates>
      <xsl:sort select="@time" order="descending" />
      <!-- Calculate $max-tests to normalize the bar sizes, account
           for different number of tests.  -->
      <xsl:with-param name="max-tests">
        <xsl:for-each select="summary">
          <xsl:sort select="@errors + @skipped + @passed"
                    data-type="number" order="descending" />
          <xsl:if test="1 = position()">
            <xsl:value-of select="@errors + @skipped + @passed" />
          </xsl:if>
        </xsl:for-each>
      </xsl:with-param>
    </xsl:apply-templates>
    <hr />
  </xsl:template>

  <!-- Overview XML: Format an individual job.  -->
  <xsl:template match="summary">
    <xsl:param name="max-tests" />
    <div onclick="activateReport(this)"
         class="bar-container"
         report="{@report-url}">
      <xsl:call-template name="bar-table">
        <xsl:with-param name="label"        select="@time"/>
        <xsl:with-param name="maxCount"     select="$max-tests"/>
        <xsl:with-param name="errorCount"   select="@errors"/>
        <xsl:with-param name="skippedCount" select="@skipped"/>
        <xsl:with-param name="passedCount"  select="@passed"/>
      </xsl:call-template>
    </div>
  </xsl:template>

</xsl:stylesheet>

<!-- =================================================================
  eof
================================================================== -->
