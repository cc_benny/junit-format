#!/usr/bin/tclsh
# ------------------------------------------------------------------------
#       graphql-backend.tcl
# ------------------------------------------------------------------------

package require http
package require tls
http::register https 443 "::tls::socket -autoservername true"

package require json
package require zlib

# variable options - Configuration data.  See
# format-junit-for-gitlab.tcl:parse-options for how to set these
# parameters on the command line.  junit-file and html-file are used
# in other modules.
variable options; array set options {
    base-url    "https://gitlab.com"
    graphql-url "https://gitlab.com/api/graphql"
    project     cc_benny/ctk
    junit-file  test-report.xml
    html-file   test-report.html
}

# The top-level GraphQL command.  With %s for variables and the actual
# query.
variable WRAPPER {
    [{
        "operationName": "getJobs",
        "variables": {%s},
        "query": "%s"
    }]
}

# The query itself.  It is separate, so indentation is easier.
variable QUERY {
    query getJobs($fullPath: ID!, $after: String) {
        project(fullPath: $fullPath) {
            jobs(after: $after, first: 100) {
                pageInfo {
                    endCursor
                    hasNextPage
                }
                nodes {
                    artifacts {
                        nodes {
                            downloadPath
                            fileType
                        }
                    }
                    detailedStatus {
                        detailsPath
                    }
                    originalId:id
                    ref:refName
                    commit_sha:shortSha
                    name
                    created_at:createdAt
                }
            }
        }
    }
}

# graphql-get-jobs cursor - Run the query against the server.  CURSOR
# can be empty initially.  Returns the raw data.
proc graphql-get-jobs {cursor} {
    variable options
    variable WRAPPER
    variable QUERY
    set url $options(graphql-url)
    set project $options(project)
    set req [format $WRAPPER [graphql-variables $project $cursor] $QUERY]
    set req [string map {\n " "} $req]
    puts stderr "Getting $url: Jobs for $project at $cursor ..."
    #puts stderr "Req: <<$req>> ..."
    set token [http::geturl $url \
                   -query $req \
                   -type "application/json" \
                   -binary 1]
    # HTTP status as a numeric code.
    set ncode [http::ncode $token]
    # HTTP status as text, potentially with a short explanation.
    set code [http::code $token]
    set data [http::data $token]
    set location "unknown"
    catch {set location [dict get [http::meta $token] Location]}
    http::cleanup $token

    # 2XX is "ok", 3XX is a redirect.  1XX is handled by the http
    # package internally, everything else is an error.
    switch [expr {$ncode / 100}] {
        2 {return $data}
        3 {error "Redirect not supported: $ncode $location"}
        default {error $code}
    }
}

# graphql-variables project cursor - Format the needed parameters.
proc graphql-variables {project cursor} {
    lappend result [graphql-attribute fullPath [graphql-string $project]]
    lappend result [graphql-attribute after [graphql-string $cursor]]
    return [join $result ","]
}

# graphql-attribute key value - Format a key-value pair as JSON.
proc graphql-attribute {key value} {
    return "[graphql-string $key]: $value"
}

# graphql-string value - Quote a JSON string.  Does not do any
# escaping at this time, because we do not need that yet.
proc graphql-string {value} {
    return "\"$value\""
}

# get-jobs-with-junit - Get all jobs from Gitlab that have a JUnit
# artifact.  Returns the jobs in our required format.
proc get-jobs-with-junit {} {
    set cursor ""
    set all {}
    set done 0
    while true {
        set jobs [graphql-get-jobs $cursor]
        set jobs [json::json2dict $jobs]

        set jobs [dict get [lindex $jobs 0] data project jobs]
        set pageInfo [dict get $jobs pageInfo]
        set jobs [dict get $jobs nodes]

        set end $done
        incr end [llength $jobs]
        puts -nonewline stderr "Filtering $done -> $end: "
        set with_junit 0
        set done $end
        foreach job $jobs {
            foreach artifact [dict get $job artifacts nodes] {
                set type [string tolower [dict get $artifact fileType]]
                lappend job ${type}_url [dict get $artifact downloadPath]
            }
            if {[dict exists $job junit_url]} {
                set id [dict get $job originalId]
                set id [lindex [split $id "/"] end]
                lappend job id $id
                set web_url [dict get $job detailedStatus detailsPath]
                # On the artifact server the user name is replaced by
                # "-" in the path.  I.e. "/cc_benny/ctk" -> "/-/ctk".
                regsub {^/[^/]*} $web_url "/-" web_url
                lappend job web_url $web_url
                lappend all $job
                incr with_junit
            }
        }
        puts stderr "$with_junit jobs with JUnit found"

        if {[dict get $pageInfo hasNextPage]} {
            set cursor [dict get $pageInfo endCursor]
        } else {
            break
        }
    }

    return $all
}

# get-junit-xml job - Get the JUnit XML for a JOB from the website.
proc get-junit-xml {job} {
    variable options
    return [get-artifact-data $job JUNIT [dict get $job junit_url]]
}

# get-jobs-with-artifacts - test-overview/make-files.sh uses this.
proc get-jobs-with-artifacts {} {
    get-jobs-with-junit
}

# get-artifacts artifact - A subcommand that gets the ARTIFACT bytes
# for all jobs given from the website.  This is used for testing.
proc get-artifacts {artifact} {
    set jobs {}
    foreach job [get-jobs-with-junit] {
        set fname [dict get $job id]-$artifact
        puts stderr "$fname: Getting ..."
        if {[catch {
            set bytes [get-artifact-data $job $artifact]
            write-file $fname $bytes
        } eresult]} {
            flush stdout
            puts stderr "$fname: $eresult"
        }
    }
}

# get-artifact-data - Get the bytes of ARTIFACT from the website.
proc get-artifact-data {job artifact {artifact_url ""}} {
    set jobid [dict get $job id]
    variable options
    if {"" eq $artifact_url} {
        set path [dict get $job archive_url]
        set bytes [get-data $path]
        set bytes [zip-decode $bytes $artifact]
    } else {
        set path $artifact_url
        set bytes [get-data $path]
        catch {set bytes [zlib gunzip $bytes]}
    }
    if {[string length $bytes] < 100} {
        error "Incomplete data: $artifact"
    }
    return $bytes
}

# get-data url - Get a page from gitlab.com.  Uses configuration
# variables `base-url' and `project' as defaults, URL can be relative
# to these.
proc get-data {url} {
    # Relative URLs are the normal top-level call, but absolute URLs
    # may occur for redirections, which we handle as recursive calls,
    # s.b.
    set url [resolve-url $url]

    # Short-cut the URL that we get for stuff that has been deleted.
    if {[string match */unknown $url]} {
        error "Not found"
    }

    puts stderr "Getting $url ..."
    set token [http::geturl $url -binary 1]
    # HTTP status as a numeric code.
    set ncode [http::ncode $token]
    # HTTP status as text, potentially with a short explanation.
    set code [http::code $token]
    set data [http::data $token]
    set location "unknown"
    catch {set location [dict get [http::meta $token] Location]}
    http::cleanup $token

    # 2XX is "ok", 3XX is a redirect.  1XX is handled by the http
    # package internally, everything else is an error.
    switch [expr {$ncode / 100}] {
        2 {return $data}
        3 {return [get-data $location]}
        default {error $code}
    }
}

# resolve-url url - Make a URL absolute with respect to our website.
proc resolve-url {url} {
    variable options
    if {[string match *:* $url]} {
        # perfect
    } elseif {[string match /* $url]} {
        set url $options(base-url)$url
    } else {
        set url $options(base-url)/$options(project)/$url
    }
    return $url
}

# zip-decode archive_bytes member - Return the bytes of MEMBER from an
# in-memory ZIP archive.
proc zip-decode {archive_bytes member} {
    file tempfile archive .zip
    write-file $archive $archive_bytes
    set open false
    try {
        set zipf [open "| unzip -p $archive $member" rb]
        return [read $zipf]
    } finally {
        file delete $archive
        if {[info exists zipf]} {
            close $zipf
        }
    }
}

# ------------------------------------------------------------------------
#       eof
# ------------------------------------------------------------------------
