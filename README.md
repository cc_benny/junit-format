[//]: # (README.md)

junit-format
====

Stylesheet to format a JUnit XML file as HTML.

Also code to generate an overview from several test runs with
integration into Gitlab.com.  The idea here being that we can generate
an interactive overview over all past test runs in the Gitlab CI and
upload the overview as an artifact to our Gitlab CI job.

Usage
----

### [format-junit.xslt](format-junit.xslt)

This stylesheet processes two kinds of XML and produces matching HTML
from it.

#### With JUnit XML

The stylesheet creates an interactive HTML page that shows all the
Tests, their success rate, and the diagnostic output for errors and
failures.  Note that this mode so far is only tested with the output
of [TclUnit](https://gitlab.com/cc_benny/tclunit).  In this mode the
stylesheet takes the following parameters:

* `report-title` - The name of the report in the tags TITLE and H1.
  The default is "JUnit Report".

* `generate-summary` - Whether the stylesheet should generate the H1
  tag and a summary for all tests (as opposed to just a display of the
  individual testsuites).  The default is `true`.  This is intended
  for reports that should be used within an overview, where the
  summary is otherwise redundant.

* `job-id`, `job-url` - If these parameters are passed, a link to the
  `job-url` is added.  The default is not to show that link.  This is
  intended for reports that are used inside of a framework to
  integrate with the Job from which the data came.

Example call:

        $ xsltproc \
            --param generate-summary false \
            --stringparam job-id $job_id \
            --stringparam job-url $job_url \
            format-junit.xslt \
            test-report.xml > test-report.html

#### With Overview XML

The stylesheet creates an overview page, summarizing a series of test
reports and linking to them.  The HTML than integrates the individual
reports in an IFRAME.  In this mode this parameter is used:

* `overview-title` - The name of the overview in the tags TITLE and H1.
  The default is "JUnit Reports".

Example call:

        $ xsltproc \
	        --stringparam overview-title "Testresults for $project" \
            format-junit.xslt overview.xml > overview.html

### [format-junit-for-gitlab.tcl](format-junit-for-gitlab.tcl)

The is the toplevel script for generating an integrated test report
display for Gitlab.  The workflow is split into several phases for
easier testing and adaptation.  The phases communicate with pipes.
Job lists are represented as Tcl lists of dicts, where the dicts are
loosely compatible with the JSON representation of jobs in the Gitlab
API.

The general syntax is:

        $ format-junit-for-gitlab.tcl \
            backend ?options? \
            subcommand ?args...?

The parameter `backend` is `api-backend` or `graphql-backend`.graphql

#### Subcommands

The following general subcommands are implemented:

* `generate-local-job` - Generate a job object describing the Gitlab
  job in which we are currently running.

* `jobs-list` - Take a list of jobs from stdin and format it one job
  per line.  This is used to get an overview and also to count jobs,
  just by using `wc -l` on the result.

* `jobs-dump` - Take a list of jobs from stdin and format them with
  one key-value pair per line.

* `generate-overview` - Take a list of jobs from stdin and generate a
  simple XML file from it.  The result can be used with
  `format-junit.xslt` to generate an HTML file giving an overview of
  all the jobs.

In addition, the `api-backend` implements these subcommands:

* `get-jobs` - Gets a list of all jobs.

* `get-jobs-with-junit` - Get a list of all jobs that have a JUnit
  artifact according to the job metadata.

The `graphql-backend` implements these subcommands:

* `get-jobs-with-junit` - Get a list of all jobs that have a JUnit
  artifact.

* `get-artifacts artifact` Get the bytes of ARTIFACT for all jobs
  given from the website.  Place them in files named in the pattern
  `${job-id}-${artifact}`.  This is used for testing.

#### Options

Options are project specific settings that are passed either as
environment variables (named after the pattern `OPTION_VAR`) or as
parameters on the command-line (in the form `--option-var value`).
The defaults of the options are the values for the [CTk
project](https://gitlab.com/cc_benny/ctk).

The following general options are defined:

* `project` - The formal Gitlab project name like `cc_benny/ctk`.
* `junit-file` - The name of the XML JUnit report artifact.
* `html-file` - The name of the HTML JUnit report artifact.

The following additional option is defined for `api-backend`:

* `personal-token` - The access token to login to the API.

Example call:

         $ format="format-junit-for-gitlab.tcl graphql-backend"
         $ ($format get-jobs-with-artifacts
            echo ""
            $format generate-local-job) \
               | $format generate-overview
               > overview.xml

License
----

GPL 3.0 or later.  Or as the legalese goes:

The code in this project "junit-format" is free software: you can
redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

A copy of the GNU General Public License is in the file
[COPYING](COPYING).  If you do not see this file here, please refer to
[https://www.gnu.org/licenses/](https://www.gnu.org/licenses/).

Todos
----

* Make Javascript-Code to find the latest overview.html.  We want this
  while tests fail.  But failing tests mark the pipeline "failed",
  too, and the regular syntax for "latest artifacts" only finds jobs
  in the latest successfull pipeline.  Javascript code accessing the
  API could do it.

* Fold the job overflow, i.e. put an opener around jobs 5-N.  Maybe
  use an alternative opener text, like "..." vs. "<<<".  Maybe have
  the opener to the left on top of the bar.  How should this interact
  with branches?

* Alternatively only show the latest 5 jobs.  How should this interact
  with branches?

----
[//]: # (eof)
