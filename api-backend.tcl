# ------------------------------------------------------------------------
#   api-backend.tcl
#
#   A backend for format-junit-for-gitlab.tcl that uses the Gitlab API
#   to get the data.  This requires an authentication key.
# ------------------------------------------------------------------------

package require rest
package require json
package require http

package require tls
http::register https 443 "::tls::socket -autoservername true"

# variable options - Configuration data.  See
# format-junit-for-gitlab.tcl:parse-options for how to set these
# parameters on the command line.
variable options; array set options {
    base-url        "https://gitlab.com/api/v4"
    personal-token  "not set"
    project         cc_benny/ctk
    junit-file      test-report.xml
    html-file       test-report.html
}

# config - Create a configuration dict for use with the rest package.
proc config {} {
    variable options
    return [list \
                headers [list Private-Token $options(personal-token)] \
                method GET]
}

# get-jobs - A subcommand that gets a list of all jobs using the API.
proc get-jobs {} {
    variable options
    set project [http::quoteString $options(project)]
    set url $options(base-url)/projects/$project/jobs
    set jobs [json::json2dict [rest::get $url {} [config]]]

    # Transform the nested "commit short_id" to "commit_sha".
    set reformed_jobs {}
    foreach job $jobs {
        lappend job commit_sha [dict get $job commit short_id]
        lappend reformed_jobs $job
    }
    return $reformed_jobs
}

# get-artifact-data - Get the artifact bytes using the API.
proc get-artifact-data {job artifact} {
    variable options
    set jobid [dict get $job id]
    set project [http::quoteString $options(project)]
    set url $options(base-url)/projects/$project/
    append url jobs/$jobid/artifacts/$artifact
    return [rest::get $url {} [config]]
}

# get-artifact - Get the artifact info for JOB and artifact TYPE.
# Return an empty dict, if not found
proc get-artifact {job type} {
    foreach artifact [dict get $job artifacts] {
        if {$type eq [dict get $artifact file_type]} {
            return $artifact
        }
    }
    return {}
}

# get-jobs-with-junit - A subcommand that gets a list of all jobs that
# have a JUnit artifact according to the job metadata.
proc get-jobs-with-junit {} {
    set jobs {}
    foreach job [get-jobs] {
        set artifact [get-artifact $job junit]
        if {{} ne $artifact} {
            lappend jobs $job
        }
    }

    return $jobs
}

# get-junit-xml job - Get the JUnit XML for a JOB using the API.
proc get-junit-xml {job} {
    set artifact [get-artifact $job junit]
    if {[dict get $artifact size] < 100} {
        error "Incomplete JUnit file"
    }
    variable options
    return [get-artifact-data $job $options(junit-file)]
}

# ------------------------------------------------------------------------
#   eof
# ------------------------------------------------------------------------
